<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.88 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.10 -->
# droid 0.3.10 - OUTDATED because got merged into ae.droid package

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_droid/develop?logo=python)](
    https://gitlab.com/ae-group/ae_droid)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_droid/release0.3.9?logo=python)](
    https://gitlab.com/ae-group/ae_droid/-/tree/release0.3.9)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_droid)](
    https://pypi.org/project/ae-droid/#history)

>ae namespace module portion droid: android constants and helper functions.

[![Coverage](https://ae-group.gitlab.io/ae_droid/coverage.svg)](
    https://ae-group.gitlab.io/ae_droid/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_droid/mypy.svg)](
    https://ae-group.gitlab.io/ae_droid/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_droid/pylint.svg)](
    https://ae-group.gitlab.io/ae_droid/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_droid)](
    https://gitlab.com/ae-group/ae_droid/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_droid)](
    https://gitlab.com/ae-group/ae_droid/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_droid)](
    https://gitlab.com/ae-group/ae_droid/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_droid)](
    https://pypi.org/project/ae-droid/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_droid)](
    https://gitlab.com/ae-group/ae_droid/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_droid)](
    https://libraries.io/pypi/ae-droid)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_droid)](
    https://pypi.org/project/ae-droid/#files)


## installation


execute the following command to install the
ae.droid module
in the currently active virtual environment:
 
```shell script
pip install ae-droid
```

if you want to contribute to this portion then first fork
[the ae_droid repository at GitLab](
https://gitlab.com/ae-group/ae_droid "ae.droid code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_droid):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_droid/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.droid.html
"ae_droid documentation").
